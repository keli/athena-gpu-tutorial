//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//

// Athena include(s).
#include "AthCUDACore/Info.h"

// System include(s).
#include <iostream>

int main() {

   // Get some information about the available CUDA GPU(s).
   auto info = AthCUDA::Info::instance();

   // Print a summary about it/them.
   std::cout << "The available GPU(s) are:\n\n" << info << std::endl;

   // Return gracefully.
   return 0;
}
