// Dear emacs, this is -*- c++ -*-
//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//
#ifndef CUDAEXAMPLES_CUDAFUNCTION_H
#define CUDAEXAMPLES_CUDAFUNCTION_H

// Athena include(s).
#include "AthCUDAInterfaces/StreamHolder.h"

// System include(s).
#include <vector>

namespace AthCUDATutorial {

   /// Simple function doing "something" using CUDA
   void cudaFunction( const std::vector< float >& a, std::vector< float >& b,
                      AthCUDA::StreamHolder& streamHolder );

} // namespace AthCUDATutorial

#endif // CUDAEXAMPLES_CUDAFUNCTION_H
