//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//

// Local include(s).
#include "SingleAlgorithm.h"
#include "cudaFunction.h"

// Tutorial include(s).
#include "CUDAExampleCommon/cuda_sc_check.h"

// Athena include(s).
#include "StoreGate/ReadHandle.h"
#include "AthCUDACore/Info.h"

// CUDA include(s).
#include <cuda_runtime_api.h>

// System include(s).
#include <vector>

namespace AthCUDATutorial {

   StatusCode SingleAlgorithm::initialize() {

      // Access the AthCUDA::Info singleton.
      const AthCUDA::Info& info = AthCUDA::Info::instance();

      // Make sure that at least a single CUDA device is available.
      if( info.nDevices() == 0 ) {
         ATH_MSG_ERROR( "At least a single CUDA device is necessary for this "
                        "algorithm!" );
         return StatusCode::FAILURE;
      }

      // Greet the user.
      ATH_MSG_INFO( "CUDA device(s) available:\n" << info );

      // Make sure that all devices are set to blocking synchronization.
      for( int i = 0; i < info.nDevices(); ++i ) {
         CUDA_SC_CHECK( cudaSetDevice( i ) );
         CUDA_SC_CHECK( cudaSetDeviceFlags( cudaDeviceScheduleBlockingSync ) );
      }

      // Initialise the algorithm's resources.
      ATH_CHECK( m_eiKey.initialize() );
      ATH_CHECK( m_streamPool.retrieve() );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode SingleAlgorithm::execute( const EventContext& ctx ) const {

      // Retrieve the event info.
      SG::ReadHandle< xAOD::EventInfo > ei( m_eiKey, ctx );

      // Generate some numbers based on the event information.
      std::vector< float > a, b;
      static const std::size_t ARRAY_SIZE = 1000;
      a.reserve( ARRAY_SIZE ); b.resize( ARRAY_SIZE );
      for( std::size_t i = 0; i < ARRAY_SIZE; ++i ) {
         a.push_back( 1.23f + 1.2f * ( ei->eventNumber() +
                                       ei->runNumber() ) );
      }

      // Perform a parallel calculation.
      auto stream = m_streamPool->getAvailableStream();
      cudaFunction( a, b, stream );

      // Print some of the array elements for debugging.
      for( std::size_t i = 0; i < 10; ++i ) {
         ATH_MSG_DEBUG( "a[ " << i << " ] = " << a[ i ] << "; b[ " << i
                        << " ] = " << b[ i ] );
      }

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

} // namespace AthCUDATutorial
