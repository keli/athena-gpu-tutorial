# Athena CUDA Examples

On one of the `aiatlas-gpu-XX` machines you can set up the build of the packages
in this directory with the following:

```sh
setupATLAS
source ./athena-gpu-tutorial/setup_cuda_env.sh
mkdir build
cd build/
cmake ../athena-gpu-tutorial/
```

Note that this setup will use CUDA 11.3 installed locally (under `/usr/local`)
on the nodes to build the device code.

## Packages

  - [CUDAExampleCommon](CUDA/CUDAExampleCommon) holds some helper code that is
    used by the other packages. You do not need to worry about it too much.
  - [CUDAExample01](CUDA/CUDAExample01) compiles a "standalone" application,
    which runs outside of an `athena.py` job.
  - [CUDAExample02](CUDA/CUDAExample02) demonstrates how to write a single
    algorithm that interacts with StoreGate, and performs some calculation on
    a GPU.
  - [CUDAExample03](CUDA/CUDAExample03) demonstrates how to have 2 algorithms
    pass data held in the GPU's memory, between each other.
