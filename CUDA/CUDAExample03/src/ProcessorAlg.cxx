//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//

// Local include(s).
#include "ProcessorAlg.h"
#include "caloClusterMaker.h"

// Tutorial include(s).
#include "CUDAExampleCommon/cuda_sc_check.h"

// Athena include(s).
#include "StoreGate/ReadHandle.h"
#include "StoreGate/WriteHandle.h"
#include "AthCUDACore/Info.h"
#include "xAODCaloEvent/CaloClusterAuxContainer.h"

// CUDA include(s).
#include <cuda_runtime_api.h>

// System include(s).
#include <memory>

namespace AthCUDATutorial {

   StatusCode ProcessorAlg::initialize() {

      // Access the AthCUDA::Info singleton.
      const AthCUDA::Info& info = AthCUDA::Info::instance();

      // Make sure that at least a single CUDA device is available.
      if( info.nDevices() == 0 ) {
         ATH_MSG_ERROR( "At least a single CUDA device is necessary for this "
                        "algorithm!" );
         return StatusCode::FAILURE;
      }

      // Greet the user.
      ATH_MSG_INFO( "CUDA device(s) available:\n" << info );

      // Make sure that all devices are set to blocking synchronization.
      for( int i = 0; i < info.nDevices(); ++i ) {
         CUDA_SC_CHECK( cudaSetDevice( i ) );
         CUDA_SC_CHECK( cudaSetDeviceFlags( cudaDeviceScheduleBlockingSync ) );
      }

      // Initialise the algorithm's resources.
      ATH_CHECK( m_gpuMemKey.initialize() );
      ATH_CHECK( m_caloClusterKey.initialize() );
      ATH_CHECK( m_streamPool.retrieve() );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode ProcessorAlg::execute( const EventContext& ctx ) const {

      // Access the GPU memory used for input.
      SG::ReadHandle< GPUMemoryBlob > gpuMem( m_gpuMemKey, ctx );

      // Grab the CUDA stream that the calculation should be executed on.
      auto stream = m_streamPool->getAvailableStream();
      if( ! stream ) {
         ATH_MSG_ERROR( "Could not get available stream for the algorithm!" );
         return StatusCode::FAILURE;
      }

      // Create the output container of the calculation.
      auto clusters = std::make_unique< xAOD::CaloClusterContainer >();
      auto clustersAux = std::make_unique< xAOD::CaloClusterAuxContainer >();
      clusters->setStore( clustersAux.get() );

      // Call the function that creates the calo clusters using the GPU.
      std::vector< float > clusterE;
      caloClusterMaker( gpuMem->ptr(), gpuMem->size(), clusterE, stream );

      // Create dummy clusters with the returned energies.
      for( float ene : clusterE ) {
         clusters->push_back( new xAOD::CaloCluster() );
         clusters->back()->setE( ene );
      }

      // Record the output object into the event store.
      SG::WriteHandle< xAOD::CaloClusterContainer >
         caloCluster( m_caloClusterKey, ctx );
      ATH_CHECK( caloCluster.record( std::move( clusters ),
                                     std::move( clustersAux ) ) );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

} // namespace AthCUDATutorial
