// Dear emacs, this is -*- c++ -*-
//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//
#ifndef CUDAEXAMPLES_PROCESSORALG_H
#define CUDAEXAMPLES_PROCESSORALG_H

// Local include(s).
#include "CUDAExample03/GPUMemoryBlob.h"

// Gaudi include(s).
#include "GaudiKernel/ServiceHandle.h"

// Athena include(s).
#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "StoreGate/ReadHandleKey.h"
#include "StoreGate/WriteHandleKey.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "AthCUDAInterfaces/IStreamPoolSvc.h"

namespace AthCUDATutorial {

   /// Algorithm processing data on the GPU, outputting data on the host
   class ProcessorAlg : public AthReentrantAlgorithm {

   public:
      /// Use the base class's constructor
      using AthReentrantAlgorithm::AthReentrantAlgorithm;

      /// @name Function(s) inherited from @c AthReentrantAlgorithm
      /// @{

      /// Function initialising the algorithm
      virtual StatusCode initialize() override;

      /// Function executing the algorithm for a specific event
      virtual StatusCode execute( const EventContext& ctx ) const override;

      /// @}

   private:
      /// @name Algorithm properties
      /// @{

      /// Key for reading the @c AthCUDATutorial::GPUMemoryBlob object with
      SG::ReadHandleKey< GPUMemoryBlob > m_gpuMemKey{ this, "GPUMemoryKey",
         "GPUMemory",
         "Key to read the AthCUDATutorial::GPUMemoryBlob object with" };

      /// Key for writing the @c xAOD::CaloClusterContainer container with
      SG::WriteHandleKey< xAOD::CaloClusterContainer > m_caloClusterKey{ this,
         "CaloClusterKey", "GPUProducedClusters",
         "Key to write the xAOD::CaloClusterContainer container with" };

      /// Handle to the CUDA stream pool
      ServiceHandle< AthCUDA::IStreamPoolSvc > m_streamPool{ this,
         "StreamPoolSvc", "AthCUDA::StreamPoolSvc",
         "Service providing CUDA streams to the algorithm" };

      /// @}

   }; // class ProcessorAlg

} // namespace AthCUDATutorial

#endif // CUDAEXAMPLES_PROCESSORALG_H
