// Dear emacs, this is -*- c++ -*-
//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//
#ifndef CUDAEXAMPLES_CALOCLUSTERMAKER_H
#define CUDAEXAMPLES_CALOCLUSTERMAKER_H

// Athena include(s).
#include "AthCUDAInterfaces/StreamHolder.h"

// System include(s).
#include <cstddef>
#include <vector>

namespace AthCUDATutorial {

   /// Simple function doing "something" using CUDA
   void caloClusterMaker( const void* gpuMem, std::size_t gpuSize,
                          std::vector< float >& clusterE,
                          AthCUDA::StreamHolder& streamHolder );

} // namespace AthCUDATutorial

#endif // CUDAEXAMPLES_CALOCLUSTERMAKER_H
