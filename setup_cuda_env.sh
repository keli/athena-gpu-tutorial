# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Script setting up a build/runtime environment to use for the CUDA
# examples of the tutorial.
#

# Set up Athena-22.0.34.
asetup Athena,22.0.34

# Point CMake at the local installation of CUDA 11.3.
export CUDACXX=/usr/local/cuda-11.3/bin/nvcc
