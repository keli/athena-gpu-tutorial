# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Script setting up a build/runtime environment to use for the ROCm/HIP
# examples of the tutorial.
#

# Set up Athena-22.0.34.
asetup Athena,22.0.34

# Point CMake and HIP at the CVMFS installation of CUDA 11.2.
export CUDA_PATH=/cvmfs/sft.cern.ch/lcg/contrib/cuda/11.2/x86_64-centos7
source ${CUDA_PATH}/setup.sh

# Point CMake at the local installation of ROCm/HIP 4.1.
export HIP_PATH=/opt/rocm-4.1.0/hip
export HIP_PLATFORM=nvidia
