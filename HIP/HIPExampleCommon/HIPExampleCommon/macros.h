// Dear emacs, this is -*- c++ -*-
//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//
#ifndef HIPEXAMPLECOMMON_MACROS_H
#define HIPEXAMPLECOMMON_MACROS_H

// HIP include(s).
#include <hip/hip_runtime_api.h>

// System include(s).
#include <iostream>
#include <stdexcept>
#include <string>

/// Helper macro for handling @c hipError_t return types "Athena functions"
#define HIP_SC_CHECK(EXP)                                                      \
   do {                                                                        \
      const hipError_t he = EXP;                                               \
      if( he != hipSuccess ) {                                                 \
         ATH_MSG_ERROR( "Could not execute \"" #EXP "\" (reason: "             \
                        << hipGetErrorString( he ) << ")" );                   \
         return StatusCode::FAILURE;                                           \
      }                                                                        \
   } while( false )

/// Helper macro for handling @c hipError_t return types in void functions
#define HIP_EXP_CHECK(EXP)                                                     \
   do {                                                                        \
      const hipError_t he = EXP;                                               \
      if( he != hipSuccess ) {                                                 \
         std::cerr << __FILE__ << ":" << __LINE__                              \
                   << " Failed to execute: " << #EXP << " ("                   \
                   << hipGetErrorString( he ) << ")"                           \
                   << std::endl;                                               \
         throw std::runtime_error( std::string( __FILE__ ) + ":" +             \
                                   std::to_string( __LINE__ ) +                \
                                   " Failed to execute: " #EXP " ("            \
                                   + hipGetErrorString( he ) +                 \
                                   ")" );                                      \
      }                                                                        \
   } while( false )


#endif // HIPEXAMPLECOMMON_MACROS_H
